/**
  Get a list of all countries with their flag emoji from an external API.
  @returns {Promise<Array>} A promise that resolves with an array of country objects,
  each containing the country name, ISO 3166-1 alpha-2 code and the flag emoji.
  If an error occurs, the promise resolves with null.
*/

import axios from "axios";

const CACHE_TIME = 60 * 60 * 1000; // 1 hour in milliseconds
let DataCountry = null;
let CacheTime = null;

async function GetCountry() {
  if (DataCountry && CacheTime && Date.now() - CacheTime < CACHE_TIME) {
    return DataCountry;
  }

  try {
    const response = await axios.get(
      "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/index.json"
    );
    CacheTime = Date.now();
    DataCountry = response.data;
    return DataCountry;
  } catch (error) {
    console.error(error);
    return null;
  }
}

export default GetCountry;
