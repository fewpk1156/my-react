import axios from "axios";

async function GetUniversities(name = null) {
  try {
    const response = await axios.get(
      "http://universities.hipolabs.com/search?country=" + name
    );
    return response.data;
  } catch (error) {
    console.error(error);
    return null;
  }
}

export default GetUniversities;
