import React from "react";
import CardCountry from "../components/card/CardCountry";

function IndexPage() {
  return (
    <div className="container-fluid flex-grow-1 container-p-y">
      <CardCountry />
    </div>
  );
}

export default IndexPage;
