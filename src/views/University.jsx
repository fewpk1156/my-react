import React from "react";
import { useParams } from "react-router-dom";
import CardUniversity from "../components/card/CardUniversity";

function UniversityPage() {
  let { code, name } = useParams();

  return (
    <div className="container-fluid flex-grow-1 container-p-y">
      <div className="row">
        <div key={code} className="col-xxl-2 col-xl-3 col-lg-3 col-mb-3 col-sm-6">
          <div className="card mb-2" style={{ minHeight: "220px" }}>
            <div className="card-body text-center card-country">
              <img
                src={"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/" + code + ".svg"}
                alt={name}
              />
              {name}
            </div>
          </div>
        </div>
      </div>
      <CardUniversity name={name} />
    </div>
  );
}

export default UniversityPage;
