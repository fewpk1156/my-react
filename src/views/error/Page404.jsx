import React from "react";

function Page404() {
  return (
    <div className="container-xxl">
      <div className="authentication-wrapper authentication-basic container-p-y">
        <div className="authentication-inner">
          <div className="card">
            <div className="card-body text-center" style={{ fontSize: "80px" }}>
              404 <br />
              Page Not Found
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Page404;
