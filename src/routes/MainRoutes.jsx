import React, { useEffect } from "react";
import { Route, Routes } from "react-router-dom";

import IndexPage from "../views/Index";
import Page404 from "../views/error/Page404";
import UniversityPage from "../views/University";
import UsersPage from "../views/Users";

function MainRoutes() {
  return (
    <Routes>
      <Route path="/" element={<IndexPage />} />
      <Route path="/users" element={<UsersPage />} />
      <Route path="/university/:code/:name" element={<UniversityPage />} />
      <Route path="*" element={<Page404 />} />
    </Routes>
  );
}

export default MainRoutes;
