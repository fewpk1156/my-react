/** 
  This is a pagination function that takes an array of data,
  as well as optional parameters for the current page and page size.
  It returns a tuple containing the data for the current page and
  the total number of pages.

  @param Array data - The array of data to be paginated
  @param Number [currentPage=1] - The current page number
   Number [pageSize=10] - The number of items per page

  returns @param Array - A tuple containing the current page data and total number of pages
*/
export function paginationData(data, currentPage = 1, pageSize = 10) {
  const totalPages = Math.ceil(data?.length / pageSize);
  currentPage = currentPage > totalPages ? 1 : currentPage;
  const startIndex = (currentPage - 1) * pageSize;
  const endIndex = startIndex + pageSize;
  const currentPageData = data?.slice(startIndex, endIndex);
  return [currentPageData, totalPages];
}

/**
 * Renders pagination links for the given number of total pages.
 *
 * @param {number} totalPages - The total number of pages to display.
 * @param {number} currentPage - The current page number.
 * @param {Function} onClick - The function to be called when a page link is clicked.
 * @returns {JSX.Element} The rendered pagination links.
 */
export function renderPagination(totalPages, currentPage = 1, limit = 30, onClick = () => {}) {
  if (!totalPages) return "";
  let pageNumbers = Array.from({ length: totalPages }, (_, i) => i + 1);
  return pageNumbers.map((pageNumber) => (
    <li key={pageNumber + "-li"} className="page-item">
      <a
        key={pageNumber}
        className={pageNumber == currentPage ? "page-link active" : "page-link"}
        onClick={() => onClick(pageNumber)}
      >
        {pageNumber}
      </a>
    </li>
  ));
}
