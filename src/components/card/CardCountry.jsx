import React, { useState, useEffect, useMemo } from "react";
import GetCountry from "../../service/Country";
import { NavLink, useSearchParams } from "react-router-dom";
import { paginationData, renderPagination } from "../../function/helpers";

function CardCountry() {
  const [data, setData] = useState();
  const [filter, setFilter] = useState();
  const [currentPage, setCurrentPage] = useState(1);
  const [searchParams, setSearchParams] = useSearchParams();

  const paginationClick = (page) => {
    setCurrentPage(page);
  };

  useEffect(() => {
    async function fetchData() {
      const json = await GetCountry();
      setData(json);
    }
    fetchData();
  }, []);

  const dataFilter = useMemo(() => {
    if (!data) return null;
    if (!filter) return data;
    let _filter = filter.toLowerCase();
    return data.filter((item) => item.code.toLowerCase().includes(_filter) || item.name.toLowerCase().includes(_filter));
  }, [data, filter]);

  let currentPageFilter = currentPage;

  if (dataFilter && searchParams.get("filter") !== filter) {
    setFilter(searchParams.get("filter"));
    paginationClick(1);
  }

  const [pageData, totalPages] = paginationData(dataFilter, currentPageFilter, 18);

  return (
    <>
      <h6>
        ( Total <span className="badge bg-primary mb-2">{totalPages.toString()}</span> pages / All filters :{" "}
        <span className="badge bg-primary mb-2">{dataFilter?.length.toString()}</span> countries )
      </h6>

      <h6>
        <a href="https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/index.json" target="_blank">
          https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/index.json
        </a>
      </h6>

      <div className="row">
        {pageData?.map((item, index) => (
          <div key={index} className="col-xxl-2 col-xl-3 col-lg-3 col-mb-3 col-sm-6">
            <div className="card mb-2" style={{ minHeight: "280px" }}>
              <div className="card-body text-center card-country">
                <NavLink to={`/university/${item.code}/${item.name}`} style={{ width: "100%" }}>
                  <img src={item.image} alt={item.name} />
                  {item.name} ({item.code})
                </NavLink>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className="row mt-2">
        <div className="col-12">
          <nav aria-label="...">
            <ul className="pagination">
              <li className="page-item">
                <a
                  className="page-link"
                  key="previous"
                  onClick={() => paginationClick(currentPageFilter - 1 > 0 ? currentPageFilter - 1 : 1)}
                >
                  Previous
                </a>
              </li>
              {renderPagination(totalPages, currentPageFilter, 10, paginationClick)}
              <li className="page-item">
                <a
                  className="page-link"
                  key="next"
                  onClick={() =>
                    paginationClick(currentPageFilter + 1 > totalPages ? currentPageFilter : currentPageFilter + 1)
                  }
                >
                  Next
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </>
  );
}

export default CardCountry;
