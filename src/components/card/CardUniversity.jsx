import React, { useState, useEffect, useMemo } from "react";
import GetUniversities from "../../service/Universities";
import { paginationData, renderPagination } from "../../function/helpers";

function CardUniversity({ name }) {
  const validSearch = new RegExp(/^[a-zA-Z]+/g);
  const [data, setData] = useState();
  const [filter, setFilter] = useState("");
  const [sort, setSort] = useState();
  const [currentPage, setCurrentPage] = useState(1);

  const addFilter = (event) => {
    setFilter(event.target.previousElementSibling.value), setCurrentPage(1);
  };

  const searchKeyUp = (event) => {
    if (event.which == 13) {
      setFilter(event.target.value), setCurrentPage(1);
    }
  };

  const handleChange = (event) => {
    event.target.value = event.target.value.match(validSearch);
  };

  const paginationClick = (page) => {
    setCurrentPage(page);
  };

  const sortableClick = (event) => {
    event.target.attributes.getNamedItem("data-sort") && "acs" == event.target.attributes.getNamedItem("data-sort").value
      ? (event.target.setAttribute("data-sort", "desc"),
        setSort([`${event.target.id}`, "desc"]),
        (event.target.firstElementChild.innerHTML = '<i class="fa-solid fa-sort-down"></i>'))
      : (event.target.setAttribute("data-sort", "acs"),
        setSort([`${event.target.id}`, "acs"]),
        (event.target.firstElementChild.innerHTML = '<i class="fa-solid fa-sort-up"></i>'));
  };

  useEffect(() => {
    async function fetchData() {
      const json = await GetUniversities(name);
      setData(json);
    }
    fetchData();
  }, []);

  const dataFilter = useMemo(() => {
    if (!data) return null;
    if (!filter) return data;
    let _filter = filter.toLowerCase();
    return data?.filter(
      (item) => item.name?.toLowerCase().includes(_filter) || item["state-province"]?.toLowerCase().includes(_filter)
    );
  }, [data, filter]);

  let currentPageFilter = currentPage;

  const [pageData, totalPages] = paginationData(dataFilter, currentPage, 15);

  if (sort) {
    dataFilter?.sort(function (a, b) {
      if (sort[1] == "acs") return a[sort[0]]?.localeCompare(b[sort[0]]);
      if (sort[1] == "desc") return b[sort[0]]?.localeCompare(a[sort[0]]);
    });
  }

  return (
    <div className="row">
      <div className="col-12">
        <h6>
          <a href={"http://universities.hipolabs.com/search?country=" + name} target="_blank">
            http://universities.hipolabs.com/search?country={name}
          </a>
        </h6>
      </div>
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <div className="row">
              <div className="col-12 mb-3">
                <div className="input-group">
                  <input
                    id="search-box"
                    type="text"
                    className="form-control"
                    onKeyUp={searchKeyUp}
                    onChange={handleChange}
                  />
                  <button className="btn btn-primary" type="button" id="btn-tb-search" onClick={addFilter}>
                    <i className="fa-solid fa-magnifying-glass"></i> Search
                  </button>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th id={"name"} className="sortable" onClick={sortableClick}>
                        Name
                        <span className="sort ms-3">
                          <i className="fa-solid fa-minus"></i>
                        </span>
                      </th>
                      <th id={"state-province"} className="sortable" onClick={sortableClick}>
                        State/Province
                        <span className="sort ms-3">
                          <i className="fa-solid fa-minus"></i>
                        </span>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {pageData?.map((item, index) => (
                      <tr key={"key-" + index}>
                        <td>{index + 1}</td>
                        <td>
                          {item.name}{" "}
                          {item.web_pages[0] ? (
                            <a href={item.web_pages[0]} target="_blank">
                              <i className="fa-solid fa-share-from-square fa-sm"></i>
                            </a>
                          ) : (
                            ""
                          )}
                        </td>
                        <td>{item["state-province"]}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
              <div className="col-12 mt-3">
                <nav aria-label="...">
                  <ul className="pagination ">
                    <li className="page-item">
                      <a
                        className="page-link"
                        key="previous"
                        onClick={() => paginationClick(currentPageFilter - 1 > 0 ? currentPageFilter - 1 : 1)}
                      >
                        Previous
                      </a>
                    </li>
                    {renderPagination(totalPages, currentPageFilter, 10, paginationClick)}
                    <li className="page-item">
                      <a
                        className="page-link"
                        key="next"
                        onClick={() =>
                          paginationClick(currentPageFilter + 1 > totalPages ? currentPageFilter : currentPageFilter + 1)
                        }
                      >
                        Next
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CardUniversity;
