import React from "react";

import { NavLink, useMatch } from "react-router-dom";

function Menu() {
  function LinkMenu({ to, children }) {
    const match = useMatch(to);

    return (
      <li key="linkIndex" className={match ? "menu-item active open" : "menu-item"}>
        <NavLink to={to} className="menu-link">
          {children}
        </NavLink>
      </li>
    );
  }

  return (
    <>
      <aside id="layout-menu" className="layout-menu menu-vertical menu bg-menu-theme" data-bg-class="bg-menu-theme">
        <div className="app-brand demo">
          <NavLink to="/" className="app-brand-link">
            <span className="app-brand-logo demo">
              <img src="/assets/img/favicon/favicon.ico" alt="" />
            </span>
            <span className="app-brand-text demo menu-text fw-bolder ms-2">Sneat</span>
          </NavLink>

          <a className="layout-menu-toggle menu-link text-large ms-auto d-xl-none">
            <i className="bx bx-chevron-left bx-sm align-middle"></i>
          </a>
        </div>

        <div className="menu-inner-shadow"></div>

        <ul className="menu-inner py-1 ps">
          <LinkMenu to="/" key="linkIndex-a">
            <i className="menu-icon fa-solid fa-house fa-sm"></i>
            <div>Universities</div>
          </LinkMenu>
          <LinkMenu to="/users" key="linkUsers-a">
            <i className="menu-icon fa-regular fa-user fa-sm"></i>
            <div>Users (DummyJSON)</div>
          </LinkMenu>
        </ul>
      </aside>
    </>
  );
}

export default Menu;
