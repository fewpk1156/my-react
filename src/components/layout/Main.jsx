import React from "react";
import Menu from "../Menu";
import Header from "../Header";
import Footer from "../Footer";
import MainRoutes from "../../routes/MainRoutes";

function MainLayout() {
  return (
    <>
      <div className="layout-wrapper layout-content-navbar">
        <div className="layout-container">
          <Menu />
          <div className="layout-page">
            <Header />
            <div className="content-wrapper">
              <MainRoutes />
              <Footer />
              <div className="content-backdrop fade"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default MainLayout;
